
devices_hands = {
	"hands/BarrettHand280/BarrettHand280.wc.xml",
	"hands/PG70/PG70.wc.xml",
	"hands/PriisholmHand/PriisholmHand.wc.xml",
	"hands/SchunkHandV2/SchunkHand.wc.xml"}

devices_humanoid = {
	"humanoid/HumanoidTestDoll.wc.xml"}
	
devices_mobile = {
	"mobile/mobiledev/mobiledev.wc.xml",
	"mobile/SegwayRMP200/SegwayRMP200.wc.xml"}

devices_parallel = {
	"paralleldev/DT-VGT/MainVGT.wc.xml",
	"paralleldev/DT-VGT/EndVGT.wc.xml",
	"paralleldev/hexapod/hexapod_jens.wc.xml"}	
	
devices_serial = {
	"serialdev/EasyBot/easybot.wc.xml",
	"serialdev/Fanuc-LRM200i/lrm200i.wc.xml",
	"serialdev/Fanuc-M710i/fanuc_m710.wc.xml",
	"serialdev/katana/Katana.wc.xml",
	"serialdev/KawasakiFS10E/KawasakiFS10E.wc.xml",
	"serialdev/KukaKR16/KukaKR16.wc.xml",
	"serialdev/",
	"serialdev/",
}

-- function that loads a device and captures screenshots of it from different angles


-- function that generates html output of images and description

